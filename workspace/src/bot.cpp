#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

#include "DatosMemCompartida.h"

int main(){
    int fd; 
    DatosMemCompartida* pMemComp;
    
    //Abrir fichero proyectado
    fd=open("/tmp/datosCompartidos",O_RDWR);
    if (fd<0){
    perror("Error al abrir el bot fichero");
    return 1;
    }
    
    //proyectar en memoria el fichero
pMemComp=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
    
    if(pMemComp==MAP_FAILED){
     perror("Error en la proyección del fichero");
     close (fd);
     return 1;
    }
    //cerramos fichero
    close (fd);
    
    
    while(1){
    
    float posRaqueta1;
    
    posRaqueta1=((pMemComp->raqueta1.y2 + pMemComp->raqueta1.y1)/2);
    
    if(posRaqueta1<pMemComp->esfera.centro.y)
        pMemComp->accion=1;
    else if(posRaqueta1>pMemComp->esfera.centro.y)
        pMemComp->accion=-1;
    else
        pMemComp->accion=0;    
    
    usleep(25000);
    }
    
   unlink("/tmp/datosCompartidos");
   close(fd);   
   return 1; 
}

