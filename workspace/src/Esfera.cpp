// Esfera.cpp: implementation of the Esfera class.
//Autor: Gema Novillo
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	pulso=0.3;
	radio_max=0.7f;
	radio_min=0.1f;
	rojo=255;
	verde=255;
	azul=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(rojo,verde,azul);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t){

	centro.x= centro.x + velocidad.x*t;
	centro.y= centro.y + velocidad.y*t;
	
	if(radio>radio_max)
	pulso= -pulso;
	
	if(radio<radio_min)
	pulso= -pulso;
	
   radio +=pulso*t;
   	
	
}

void Esfera::cambiarColor(unsigned char r,unsigned char v,unsigned char a){
	rojo =r;
	verde =v;
	azul=a;

}

