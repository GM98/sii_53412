 #include <sys/types.h>
 #include <sys/stat.h>
 #include <fcntl.h>
#include <unistd.h>
 #include <stdio.h>
#include "estructura.h"



int main() {
int fd;
puntuacion puntuacion;
//Crea FIFO
	if(mkfifo("FIFO_LOGGER",0660)<0){
	   perror("no puede crearse el FIFO");
	   return 1;
	}
//Abro FIFO
	if((fd=open("FIFO_LOGGER",O_RDONLY))<0){
	   perror("no puede abrirse el FIFO");
	   return 1;
	}
//Lectura
  while( read(fd,&puntuacion,sizeof(puntuacion))  ==sizeof(puntuacion)) {
	if(puntuacion.numJugador==1){
	printf("Jugador 1 marca 1 punto, lleva un total de %d   puntos\n",puntuacion.jugador1);
  }
  if(puntuacion.numJugador==2){
	printf("Jugador 2 marca 1 punto, lleva un total de %d   puntos\n",puntuacion.jugador2);
  }
  if (puntuacion.ganador1==true){
  printf("Jugador 1 lleva un total de %d puntos y ha ganado la partida\n",puntuacion.jugador1);
  }
  if (puntuacion.ganador2==true){
  printf("Jugador 2 lleva un total de %d puntos y ha ganado la partida\n",puntuacion.jugador2);
  }
  }
                
close(fd);
unlink("FIFO_LOGGER");
return 0;

}
